package se331.lab.rest.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import se331.lab.rest.entity.Student;

import java.util.ArrayList;
import java.util.List;

@Controller
public class StudentController {
    List<Student> students;
        public StudentController() {

        this.students = new ArrayList<>();
        this.students.add(Student.builder()
        .id((long) 11)
        .studentId("SE-001")
        .name("Prayuth")
        .surname("The minister")
        .gpa(3.59)
        .image("https://firebasestorage.googleapis.com/v0/b/test-331-lab05.appspot.com/o/tu.jpg?alt=media&token=f16b5c1c-fbea-4d98-9fa3-732b69a1bae8")
        .penAmount(15)
        .description("The great man ever!")
        .build());

        this.students.add(Student.builder()
        .id((long) 21)
        .studentId("SE-002")
        .name("Cherprang")
        .surname("BNK48")
        .gpa(4.01)
        .image("https://firebasestorage.googleapis.com/v0/b/test-331-lab05.appspot.com/o/cherprang.png?alt=media&token=2e6a41f3-3bf0-4e42-ac6f-8b7516e24d92")
        .penAmount(2)
        .description("Code for Thailand")
        .build());

        this.students.add(Student.builder()
        .id((long) 31)
        .studentId("SE-003")
        .name("Nobi")
        .surname("Nobita")
        .gpa(1.77)
        .image("https://firebasestorage.googleapis.com/v0/b/test-331-lab05.appspot.com/o/nobita.jpg?alt=media&token=16e30fb0-9904-470f-b868-c35601df8326")
        .penAmount(0)
        .description("Welcome to Olympic")
        .build());

        this.students.add(Student.builder()
        .id((long) 41)
        .studentId("SE-004")
        .name("Jotaro")
        .surname("Jostar")
        .gpa(5.00)
        .image("https://i.kym-cdn.com/entries/icons/mobile/000/028/775/Screen_Shot_2019-03-06_at_4.32.48_PM.jpg")
        .penAmount(20)
        .description("you're approaching me ?")
        .build());
    }

    @GetMapping("/students")
    public ResponseEntity<List<Student>> getAllStudent() {
        return ResponseEntity.ok(students);
    
    }
    @GetMapping("/students/{id}")
    public ResponseEntity<Student> getStudentById(@PathVariable("id") Long id) {
        return ResponseEntity.ok(students.get(Math.toIntExact(id-1)));
    }
    @PostMapping("/students")
    public ResponseEntity<Student> saveStudent(@RequestBody Student student) {
        student.setId((long) this.students.size());
        this.students.add(student);
        return ResponseEntity.ok(student);
    }
}